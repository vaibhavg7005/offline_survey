package com.example.offline_survey;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FirstDetail extends AppCompatActivity {
    String result = "";
    TextView question;
    int numberOfOptions;
    EditText editText;
    RadioGroup radioGroup;
    RadioButton radioButton;
    CheckBox checkbox;
    Button submit_button;
    ConstraintLayout save_message;
    LinearLayout linearLayout;
    ArrayList<String> text = new ArrayList<String>();
    List<CheckBox> items = new ArrayList<CheckBox>();
    ArrayList<ArrayList<String>> options = new ArrayList<ArrayList<String>>();
    ArrayList<String> type = new ArrayList<String>();
    ArrayList<Boolean> isRequired = new ArrayList();
    ArrayList<String> id = new ArrayList();
    String filename;
    int jsonArrayCount;
    TextView next;
    int clicked_item_position;
    TextView previous;
    int track_call = 0;
    String resultant = "";
    int input = 0;
    String resultId = "";
    Map<String, String> resultMap = new HashMap<>();
    private String responseFile = "Answers.json";
    private String filepath = "MyFileStorage";
    String filedata = "";
    File survey_response_file;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        clicked_item_position = 0;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_detail);
        Intent intent = this.getIntent();
        if (intent != null) {
            String strdata = intent.getExtras().getString("source");
            if (strdata.equals("survey_to_enter")) {
                text = (ArrayList<String>) getIntent().getSerializableExtra("key");
                options = (ArrayList<ArrayList<String>>) getIntent().getSerializableExtra("listOfOptions");
                type = (ArrayList<String>) getIntent().getSerializableExtra("typeOf");
                id = (ArrayList<String>) getIntent().getSerializableExtra("listOfId");
                isRequired = (ArrayList<Boolean>) getIntent().getSerializableExtra("requirement");
                filename = getIntent().getStringExtra("file_name");
                jsonArrayCount = getIntent().getIntExtra("entries", 0);
                for (int i = 0; i < jsonArrayCount; i++) {
                    resultMap.put(id.get(i), "");
                }
            }
            if (strdata.equals("saved_survey")) {
                track_call = 1;
                resultant = getIntent().getStringExtra("resultIntent");
                resultId = getIntent().getStringExtra("resultId");
                int start = resultant.indexOf("file") + 5;
                int end = resultant.indexOf("\n");
                filename = resultant.substring(start, end);
                get_json();
            }
        }
        final SurveyViewModel surveyViewModel = ViewModelProviders.of(this).get(SurveyViewModel.class);
        submit_button = (Button) findViewById(R.id.submit_button);
        next = (TextView) findViewById(R.id.next);
        previous = (TextView) findViewById(R.id.previous);

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (input == jsonArrayCount) {
                    setContent(--input);
                } else {
                    delete_content();
                    setContent(--input);
                }
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.get(input).equals("input")) {
                    closeKeyboard();
                }

                if (input < jsonArrayCount) {
                    delete_content();
                    setContent(++input);
                }
            }
        });
        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result += "file=" + filename;
                result += "\n";
                for (int i = 0; i < jsonArrayCount; i++) {
                    result += id.get(i) + "=";
                    result += resultMap.get(id.get(i));
                    result += "\n";
                }
                try {
                    FileInputStream fis = new FileInputStream(survey_response_file);
                    DataInputStream in = new DataInputStream(fis);
                    BufferedReader br =
                            new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    while ((strLine = br.readLine()) != null) {
                        filedata = filedata + strLine;
                    }
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (track_call == 1) {
                    int clicked_item_pos = getIntent().getIntExtra("position", 0);
                    int a = ordinalIndexOf(filedata, "{", clicked_item_position);
                    int b = ordinalIndexOf(filedata, "}", clicked_item_position) + 1;
                    String toDelete = filedata.substring(a, b);
                    filedata = filedata.replace(toDelete, "");
                }
                try {
                    FileOutputStream fos = new FileOutputStream(survey_response_file);
                    if (!filedata.contains("]"))
                        filedata += "[]";
                    if (filedata.contains("}")) {
                        filedata = insertString(filedata, ",", filedata.lastIndexOf("}"));
                    }
                    String singleResponse = "{";

                    singleResponse += "\"filename\"" + " " + ":" + " " + "\"" + filename + "\"" + "\n";
                    for (int i = 0; i < jsonArrayCount; i++) {
                        if (type.get(i).equals("input")) {
                            if (i == jsonArrayCount - 1) {
                                singleResponse += "\"" + id.get(i) + "\"" + " " + ":" + " " + "\"" + resultMap.get(id.get(i)) + "\"";
                            } else {
                                singleResponse += "\"" + id.get(i) + "\"" + " " + ":" + " " + "\"" + resultMap.get(id.get(i)) + "\"" + "\n";
                            }
                        } else if (type.get(i).equals("select_multiple")) {
                            String response = resultMap.get(id.get(i));
                            String value = "";
                            for (int j = 0; j < response.length(); j++) {
                                if (response.charAt(j) == '1') {
                                    if (j != response.length() - 1) {
                                        value += options.get(i).get(j);
                                        value += ",";
                                    } else {
                                        value += options.get(i).get(j);
                                    }
                                }
                            }
                            if (i == jsonArrayCount - 1) {
                                singleResponse += "\"" + id.get(i) + "\"" + " " + ":" + " " + "\"" + value + "\"";

                            } else {
                                singleResponse += "\"" + id.get(i) + "\"" + " " + ":" + " " + "\"" + value + "\"" + "\n";
                            }
                        } else if (type.get(i).equals("select_one")) {
                            String response = resultMap.get(id.get(i));
                            int choice = Integer.valueOf(response) - 1;
                            String value = "";
                            value += options.get(i).get(choice);
                            if (i == jsonArrayCount - 1) {
                                singleResponse += "\"" + id.get(i) + "\"" + " " + ":" + " " + "\"" + value + "\"";

                            } else {
                                singleResponse += "\"" + id.get(i) + "\"" + " " + ":" + " " + "\"" + value + "\"" + "\n";
                            }
                        }
                    }
                    String id=UUID.randomUUID().toString();

                    singleResponse+= "\"UUID\""+" "+":"+" "+"\""+id+"\"";
                    singleResponse += "}";
                    filedata = insertString(filedata, singleResponse, filedata.length() - 2);
                    if(filedata.charAt(1)==',')
                        filedata=filedata.substring(0,1)+filedata.substring(2);
                    fos.write(filedata.getBytes());
                    fos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
                String time = dateFormat.format(new Date()).toString();
                if (track_call == 0) {
                    resultId = UUID.randomUUID().toString();

                    surveyViewModel.insert(new ResultSurvey(result, resultId, filename, time));
                } else {
                    surveyViewModel.insert(new ResultSurvey(result, resultId, filename, time));
                }
                FirstDetail.super.onBackPressed();

            }
        });

        setContent(input);
        if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
            Toast.makeText(getApplicationContext(), "External storage not available to save the data", Toast.LENGTH_LONG).show();
        } else {
            survey_response_file = new File(getExternalFilesDir(filepath), responseFile);
        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    void setContent(int input) {
        previous = (TextView) findViewById(R.id.previous);
        next = (TextView) findViewById(R.id.next);
        question = (TextView) findViewById(R.id.question);
        linearLayout = (LinearLayout) findViewById(R.id.displayOptions);
        save_message = (ConstraintLayout) findViewById(R.id.end_message);
        if (input == 0)
            previous.setVisibility(View.INVISIBLE);
        else
            previous.setVisibility(View.VISIBLE);
        if (input == jsonArrayCount) {
            linearLayout.setVisibility(View.GONE);
            save_message.setVisibility(View.VISIBLE);
            next.setVisibility(View.INVISIBLE);
            question.setVisibility(View.INVISIBLE);
            return;
        } else {
            linearLayout.setVisibility(View.VISIBLE);
            save_message.setVisibility(View.GONE);
            next.setVisibility(View.VISIBLE);
            question.setVisibility(View.VISIBLE);
        }

        linearLayout = (LinearLayout) findViewById(R.id.displayOptions);
        question = (TextView) findViewById(R.id.question);
        question.setText(text.get(input));
        question.setGravity(Gravity.START);
        if (type.get(input).equals("input")) {
            editText = new EditText(this);
            editText.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            editText.setWidth(1500);
            linearLayout.addView(editText);
            editText.setText(resultMap.get(id.get(input)));
        } else if (type.get(input).equals("select_one")) {
            numberOfOptions = options.get(input).size();
            radioGroup = new RadioGroup(this);
            int choice;
            if (resultMap.get(id.get(input)).equals("")) {
                choice = -1;
            } else {
                choice = Integer.valueOf(resultMap.get(id.get(input)));
            }
            for (int i = 0; i < numberOfOptions; i++) {

                radioButton = new RadioButton(this);
                radioButton.setText(options.get(input).get(i));
                radioGroup.addView(radioButton);
                if (choice == i + 1)
                    radioButton.setChecked(true);
                else
                    radioButton.setChecked(false);
            }
            linearLayout.addView(radioGroup);
        } else if (type.get(input).equals("select_multiple")) {
            int numberOfCheckbox = options.get(input).size();
            int choices[] = new int[numberOfCheckbox];
            if (resultMap.get(id.get(input)).equals("")) {
                for (int i = 0; i < numberOfCheckbox; i++) {
                    choices[i] = 0;
                }
            } else {
                for (int i = 0; i < numberOfCheckbox; i++) {
                    choices[i] = Character.getNumericValue(resultMap.get(id.get(input)).charAt(i));
                }
            }

            for (int i = 0; i < numberOfCheckbox; i++) {
                checkbox = new CheckBox(this);
                checkbox.setText(options.get(input).get(i));
                items.add(checkbox);
                linearLayout.addView(checkbox);
                if (choices[i] == 0) {
                    checkbox.setChecked(false);
                } else {
                    checkbox.setChecked(true);
                }
            }
        }
    }

    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    void delete_content() {
        if (type.get(input).equals("input")) {
            resultMap.put(id.get(input), editText.getText().toString());
        } else if (type.get(input).equals("select_one")) {
            resultMap.put(id.get(input), String.valueOf(numberOfOptions - radioGroup.getCheckedRadioButtonId() % numberOfOptions));
        } else if (type.get(input).equals("select_multiple")) {
            String checkInfo = new String();
            for (CheckBox item : items) {
                if (item.isChecked()) {
                    checkInfo += "1";
                } else {
                    checkInfo += "0";
                }
            }
            items.clear();
            resultMap.put(id.get(input), checkInfo);
        }
        linearLayout.removeAllViews();
    }

    public static String insertString(
            String originalString,
            String stringToBeInserted,
            int index) {
        StringBuffer newString
                = new StringBuffer(originalString);

        newString.insert(index + 1, stringToBeInserted);
        return newString.toString();
    }

    void get_json() {
        String json;
        try {
            InputStream inputStream = getAssets().open(filename);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
            JSONArray jsonArray = new JSONArray(json);
            jsonArrayCount = jsonArray.length();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                text.add(jsonObject.getString("text"));
                type.add(jsonObject.getString("type"));
                id.add(jsonObject.getString("id"));
                isRequired.add(jsonObject.getBoolean("isRequired"));
            }
            for (int i = 0; i < jsonArray.length(); i++) {
                int start = resultant.indexOf(id.get(i)) + id.get(i).length() + 1;
                int end = ordinalIndexOf(resultant, "\n", i + 1);
                String answer = resultant.substring(start, end);

                resultMap.put(id.get(i), answer);
            }
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                JSONArray optionsArray = jsonObject.getJSONArray("options");
                if (optionsArray != null && optionsArray.length() > 0) {
                    ArrayList<String> list = new ArrayList<>();
                    for (int j = 0; j < optionsArray.length(); j++) {
                        JSONObject object = optionsArray.getJSONObject(j);
                        String label = null;
                        label = object.getString("label");
                        list.add(label);
                    }
                    options.add(list);

                } else {

                    ArrayList<String> list = new ArrayList<>();
                    list.add("empty");
                    options.add(list);

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public int ordinalIndexOf(String str, String substring, int n) {
        int pos = -1;
        do {
            pos = str.indexOf(substring, pos + 1);
        } while (n-- > 0 && pos != -1);
        return pos;
    }
}