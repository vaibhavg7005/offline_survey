package com.example.offline_survey;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    String filename;
    public ArrayList<String> text = new ArrayList();
    ArrayList<String> id = new ArrayList();
    ArrayList<String> type = new ArrayList();
    ArrayList<ArrayList<String>> options_list = new ArrayList<ArrayList<String>>();
    ArrayList<Boolean> isRequired = new ArrayList();
    int jsonArrayCount;
    Button start;
    Button start_fitness;
    RecyclerView recyclerView;
    private SurveyViewModel surveyViewModel;
    SurveyListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        start = (Button) findViewById(R.id.startQuiz);
        start_fitness=(Button) findViewById(R.id.startFitnessQuiz);
        recyclerView=(RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(MainActivity.this, DividerItemDecoration.VERTICAL));
        adapter=new SurveyListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        surveyViewModel= ViewModelProviders.of(this).get(SurveyViewModel.class);
        surveyViewModel.getAllResults().observe(this, new Observer<List<ResultSurvey>>() {
          @Override
        public void onChanged(@Nullable final List<ResultSurvey> results) {
          adapter.setResults(results);
        }
        });
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filename= "General.json";
                getFileName();
                get_json();
                Intent intent = new Intent(MainActivity.this, FirstDetail.class);
                intent.putExtra("source","survey_to_enter");
                intent.putExtra("key", text);
                intent.putExtra("typeOf", type);
                intent.putExtra("listOfId",id);
                intent.putExtra("listOfOptions", options_list);
                intent.putExtra("requirement", isRequired);
                intent.putExtra("entries",jsonArrayCount);
                intent.putExtra("file_name",filename);
                startActivity(intent);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            }
        });
        start_fitness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filename= "Fitness.json";
                getFileName();
                get_json();
                Intent intent = new Intent(MainActivity.this, FirstDetail.class);
                intent.putExtra("source","survey_to_enter");
                intent.putExtra("key", text);
                intent.putExtra("typeOf", type);
                intent.putExtra("listOfId",id);
                intent.putExtra("listOfOptions", options_list);
                intent.putExtra("requirement", isRequired);
                intent.putExtra("entries",jsonArrayCount);
                intent.putExtra("file_name",filename);
                startActivity(intent);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        });
        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder,
                                         int direction) {
                        int position = viewHolder.getAdapterPosition();
                        ResultSurvey myWord = adapter.getWordAtPosition(position);
                        surveyViewModel.deleteSurvey(myWord);
                    }
                });

        helper.attachToRecyclerView(recyclerView);
    }
    public String getFileName()
    {
        return filename;
    }

    public void get_json() {
        String json;
        try {
            InputStream inputStream = getAssets().open(filename);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
            JSONArray jsonArray = new JSONArray(json);
            jsonArrayCount=jsonArray.length();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                text.add(jsonObject.getString("text"));
                type.add(jsonObject.getString("type"));
                id.add(jsonObject.getString("id"));
                isRequired.add(jsonObject.getBoolean("isRequired"));
            }
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                JSONArray optionsArray = jsonObject.getJSONArray("options");
                if (optionsArray != null && optionsArray.length() > 0) {
                    ArrayList<String> list = new ArrayList<>();
                    for (int j = 0; j < optionsArray.length(); j++) {
                        JSONObject object = optionsArray.getJSONObject(j);
                        String label = null;
                        label = object.getString("label");
                        list.add(label);
                    }
                    options_list.add(list);

                } else {

                    ArrayList<String> list = new ArrayList<>();
                    list.add("empty");
                    options_list.add(list);

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.export_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.export)
        {
            Toast.makeText(getApplicationContext(),"Data saved in external memory",Toast.LENGTH_LONG).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
