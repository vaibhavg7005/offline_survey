package com.example.offline_survey;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities={ResultSurvey.class},version=1,exportSchema = false)
public abstract class SurveyDatabase extends RoomDatabase {
    public SurveyDao surveyDao;
    private static volatile SurveyDatabase Instance;

    public abstract SurveyDao getSurveyDao();

    static SurveyDatabase getDatabase(final Context context) {
        if (Instance == null) {
            synchronized (SurveyDatabase.class) {
                if (Instance == null) {
                    Instance = Room.databaseBuilder(context.getApplicationContext(), SurveyDatabase.class, "survey_database")
                            .fallbackToDestructiveMigration().addCallback(roomDatabaseCallback).build();
                }

            }
        }
        return Instance;
    }
    private static RoomDatabase.Callback roomDatabaseCallback=new RoomDatabase.Callback(){

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase database)
        {
            super.onOpen(database);
            new PopulateDbAsync(Instance).execute();
        }
    };
    private static class PopulateDbAsync extends AsyncTask<Void,Void,Void>{
        private SurveyDao dao;
        String words[]={};
        PopulateDbAsync(SurveyDatabase database)
        {
            dao=database.getSurveyDao();
        }

        @Override
        protected Void doInBackground(final Void... Params) {
            if(dao.getAnyResult().length<1)
            {
                for(int i=0;i<=words.length-1;i++)
                {
                    ResultSurvey result=new ResultSurvey(words[i]);
                    dao.insert(result);
                }
            }
            return null;
        }
    }
}
