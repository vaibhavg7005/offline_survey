package com.example.offline_survey;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SurveyListAdapter extends RecyclerView.Adapter<SurveyListAdapter.SurveyViewHolder> {
    Context context;
    private LayoutInflater inflater;
    private List<ResultSurvey> results;
    SurveyListAdapter(Context context)
    {
        inflater=LayoutInflater.from(context);
        this.context=context;
    }
    @Override
    public SurveyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemview=inflater.inflate(R.layout.recyclerview_item,parent,false);
        return new SurveyViewHolder(itemview);
    }
    @Override
    public void onBindViewHolder(@NonNull SurveyListAdapter.SurveyViewHolder holder, final int position) {
        if(results!=null)
        {
            final ResultSurvey current=results.get(position);
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
            String formattedDate = df.format(c);
            MainActivity obj=new MainActivity();
            int mark_index=current.getFilename().indexOf(".json");
            String fileNameModified="";
            for(int i=0;i<mark_index;i++)
                fileNameModified+=current.getFilename().charAt(i);
            holder.surveyItemView.setText(fileNameModified+"\n\n"+formattedDate);
            holder.timeView.setText("\n"+"\n"+current.getTime());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context,FirstDetail.class);
                    intent.putExtra("resultIntent",current.getSurveyData());
                    intent.putExtra("source","saved_survey");
                    intent.putExtra("resultId",current.getId());
                    intent.putExtra("position",position);
                    ((MainActivity)context).startActivity(intent);
        }
    });
        }
        else
        {
            holder.surveyItemView.setText("No Word");
        }
    }
    void setResults(List<ResultSurvey>results_param)
    {
        results=results_param;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        if(results!=null)
        {
            return results.size();
        }
        else return 0;
    }
    class SurveyViewHolder extends RecyclerView.ViewHolder{
        private final TextView surveyItemView;
        private final TextView timeView;

        public SurveyViewHolder(@NonNull View itemView) {
            super(itemView);
            surveyItemView=itemView.findViewById(R.id.textView);
            timeView=itemView.findViewById(R.id.timeview);
        }
    }
    public ResultSurvey getWordAtPosition (int position) {
        return results.get(position);
    }
}
