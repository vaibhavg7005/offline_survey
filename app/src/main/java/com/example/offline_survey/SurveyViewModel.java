package com.example.offline_survey;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class SurveyViewModel extends AndroidViewModel {
    private SurveyRepository surveyRepository;
    private LiveData<List<ResultSurvey>> allResults;
    public SurveyViewModel(@NonNull Application application) {
        super(application);
        surveyRepository=new SurveyRepository(application);
        allResults=surveyRepository.getAllResults();
        }
        LiveData<List<ResultSurvey>> getAllResults() {
            return allResults;
        }
        public void insert(ResultSurvey resultSurvey)
        {
            surveyRepository.insert(resultSurvey);
        }
    public void deleteSurvey(ResultSurvey result) {surveyRepository.deleteSurvey(result);}
}
