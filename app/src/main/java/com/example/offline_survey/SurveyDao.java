package com.example.offline_survey;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface SurveyDao {
    @Delete
    void deleteSurvey(ResultSurvey resultSurvey);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ResultSurvey word);

    @Query("Select * from survey_table LIMIT 1")
    ResultSurvey[] getAnyResult();

    @Query("Select * from survey_table")
    LiveData<List<ResultSurvey>> getAllResults();
}