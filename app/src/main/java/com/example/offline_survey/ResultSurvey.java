package com.example.offline_survey;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Entity(tableName="survey_table")
public class ResultSurvey {
    @ColumnInfo(name="word")
    private String surveyData;
    @NonNull
    @PrimaryKey
    @ColumnInfo(name="surveyId")
    private String id;
    private String filename;
    private String time;

    public ResultSurvey(){}
    @Ignore
    public ResultSurvey(String word)
    {
        id=UUID.randomUUID().toString();
        surveyData=word;
    }
    public ResultSurvey(String word,String id,String filename,String time){
        surveyData=word;this.id=id;this.filename=filename;this.time=time;
    }
    public String getTime(){return time;}
    public void setTime(String time){this.time=time;}
    public String getSurveyData()
    {
        return surveyData;
    }
    public void setSurveyData(String text)
    {
        surveyData=text;
    }
    public void setFilename(String filename){this.filename=filename;}
    public String getFilename(){return filename;}
    public String getId(){return id;}
    public void setId(String id){
        this.id=id;}
}