package com.example.offline_survey;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class SurveyRepository {
    private SurveyDao msurveyDao;
    private LiveData<List<ResultSurvey>> mAllResults;
    SurveyRepository(Application application)
    {
        SurveyDatabase surveyDatabase=SurveyDatabase.getDatabase(application);
        msurveyDao=surveyDatabase.getSurveyDao();
        mAllResults=msurveyDao.getAllResults();
    }
    LiveData<List<ResultSurvey>> getAllResults(){
        return mAllResults;
    }
    public void insert(ResultSurvey resultSurvey)
    {
        new insertAsyncTask(msurveyDao).execute(resultSurvey);
    }
    private static class insertAsyncTask extends AsyncTask<ResultSurvey,Void,Void>{
        private SurveyDao asyncTaskDao;
        insertAsyncTask(SurveyDao dao)
        {
            asyncTaskDao=dao;
        }

        @Override
        protected Void doInBackground(final ResultSurvey...params) {
            asyncTaskDao.insert(params[0]);
            return null;
        }
    }
    private static class deleteSurveyAsyncTask extends AsyncTask<ResultSurvey, Void, Void> {
        private SurveyDao mAsyncTaskDao;

        deleteSurveyAsyncTask(SurveyDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final ResultSurvey... params) {
            mAsyncTaskDao.deleteSurvey(params[0]);
            return null;
        }
    }
    public void deleteSurvey(ResultSurvey result)  {
        new deleteSurveyAsyncTask(msurveyDao).execute(result);
    }

}
